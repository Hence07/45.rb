def ghost_room()
  puts "Stay tune, this is not a joke be at your maximum alert."
  puts "there is weird noise everywhere, probably not a good idea to saty around."
  puts "A staircase leads upstairs and a door is in the far corner of the room."
  puts "Will you take the stairs or open the door?"
  door_open = false

  while true
    prompt()
    next_move = gets.chomp()

    if next_move == "take the stairs"
      puts "The stairs fall apart and you crash through the floor, falling into a pit full of water in the basement."
      return :dead
    elsif next_move == "open the door" and not door_open
      puts "The door opens, It looks to be a room filled with vodoo. Will you enter the room or take the stairs?"
      door_open = true
    elsif next_move == "take the stairs" and door_open
      puts "The banister crumbles and breaks and sends you tumbling. You break your neck!"
      return :dead
    elsif next_move == "enter the room" and door_open
      return :vodoo_room
    else
      puts "you better know what you doing."
    end
  end
end
