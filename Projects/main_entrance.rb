def main_entrance()
  puts "There we go,you have entered the main entrance."
  puts "This appears to be servants quarters."
  puts "Across the room is a doorway. In the corner there is a wardrobe."
  puts "Do you want to open the door or open the wardrobe?"
  while true
    prompt()
    next_move = gets.chomp
    if next_move == "open the wardrobe"
      puts "A ghostly hand grabs you and pulls you into the wardrobe."
      puts "You fall head first through a hole in the wardrobe."
      puts "You plunge into a pit trap in the basement filled with water."
      puts "There is no way out the basement, drowning is a bad way to die."
      return :dead
    elsif next_move == "open the door"
      puts "Good choice. You may now continue."
      return :ghost_room
    else
      puts "I don\'t know which way you want to go."
    end
  end
end
